package cn.anoxia.domain.strategy.service.rule;

import cn.anoxia.domain.strategy.model.entity.RuleActionEntity;
import cn.anoxia.domain.strategy.model.entity.RuleMatterEntity;

/**
 * 规则过滤接口
 * @param <T>
 */
public interface ILogicFilter<T extends RuleActionEntity.RaffleEntity> {


    RuleActionEntity<T> filter(RuleMatterEntity ruleMatterEntity);

}
