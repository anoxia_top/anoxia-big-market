package cn.anoxia.domain.strategy.service.raffle;

import cn.anoxia.domain.strategy.model.entity.*;
import cn.anoxia.domain.strategy.model.vo.StrategyAwardRuleModelVO;
import cn.anoxia.domain.strategy.repository.IStrategyRepository;
import cn.anoxia.domain.strategy.service.armory.IStrategyDispatch;
import cn.anoxia.domain.strategy.service.rule.ILogicFilter;
import cn.anoxia.types.common.Constants;
import cn.anoxia.types.enums.LogicModel;
import cn.anoxia.types.enums.ResponseCode;
import cn.anoxia.types.enums.RuleLogicCheckTypeVO;
import cn.anoxia.types.exception.AppException;
import com.sun.deploy.security.ruleset.RuleAction;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

/**
 * 抽奖抽象类
 * 定义抽奖的模版
 */
@Slf4j
public abstract class AbstractRaffleStrategy implements IRaffleStrategy {


    protected IStrategyRepository strategyRepository;

    // 策略调度服务 -> 只负责抽奖处理，通过新增接口的方式，隔离职责，不需要使用方关心或者调用抽奖的初始化
    protected IStrategyDispatch strategyDispatch;

    public AbstractRaffleStrategy(IStrategyRepository repository, IStrategyDispatch dispatch) {
        this.strategyRepository = repository;
        this.strategyDispatch = dispatch;
    }

    /**
     * 定义模版
     * @param raffleFactorEntity 抽奖因子实体对象，根据入参信息计算抽奖结果
     * @return
     */
    @Override
    public RaffleAwardEntity performRaffle(RaffleFactorEntity raffleFactorEntity) {
        // 参数检查
        String userId = raffleFactorEntity.getUserId();
        Long strategyId = raffleFactorEntity.getStrategyId();
        if (null == strategyId || StringUtils.isBlank(userId)) {
            throw new AppException(ResponseCode.ILLEGAL_PARAMETER.getCode(),
                    ResponseCode.ILLEGAL_PARAMETER.getInfo());
        }
        StrategyEntity strategy = strategyRepository.queryStrategyEntityByStrategyId(strategyId);
        // 为什么这里要重新build一个对象
        RaffleFactorEntity raffleFactor = RaffleFactorEntity.builder()
                .userId(userId)
                .strategyId(strategyId)
                .build();
        String[] ruleModels = strategy.getRuleModels().split(Constants.SPLIT);
        RuleActionEntity<RuleActionEntity.RaffleBeforeEntity> ruleAction = this.doCheckRaffleBeforeLogic(raffleFactor, ruleModels);

        // 结束了规则处理
        if (RuleLogicCheckTypeVO.TAKE_OVER.getCode().equals(ruleAction.getCode())) {
            if (LogicModel.RULE_BLACKLIST.getCode().equals(ruleAction.getRuleModel())) {
                return RaffleAwardEntity.builder()
                        .awardId(ruleAction.getData().getAwardId())
                        .build();
            } else if (LogicModel.RULE_WIGHT.getCode().equals(ruleAction.getRuleModel())) {
                // 权重根据返回的信息进行抽奖
                RuleActionEntity.RaffleBeforeEntity raffleBeforeEntity = ruleAction.getData();
                String ruleWeightValueKey = raffleBeforeEntity.getRuleWeightValueKey();
                Integer awardId = strategyDispatch.getRandomAwardId(strategyId, ruleWeightValueKey);
                return RaffleAwardEntity.builder()
                        .awardId(awardId)
                        .build();
            }
        }

        // 4. 默认抽奖流程
        Integer awardId = strategyDispatch.getRandomAwardId(strategyId);

        // 5. 处理抽中奖后过滤
        StrategyAwardRuleModelVO ruleModelVO = strategyRepository.queryStrategyAwardRuleModelVO(strategyId, awardId);
        RaffleFactorEntity centerRaffle = RaffleFactorEntity.builder()
                .strategyId(strategyId)
                .awardId(awardId)
                .userId(userId)
                .build();
        RuleActionEntity<RuleActionEntity.RaffleCenterEntity> centerRuleAction =
                this.doCheckRaffleCenterLogic(centerRaffle, ruleModelVO.raffleCenterRuleModelList());
        if (RuleLogicCheckTypeVO.TAKE_OVER.getCode().equals(centerRuleAction.getCode())) {
            log.info("【临时日志】中奖中规则拦截，通过抽奖后规则 rule_luck_award 走兜底奖励。");
            return RaffleAwardEntity.builder()
                    .awardDesc("中奖中规则拦截，通过抽奖后规则 rule_luck_award 走兜底奖励。")
                    .build();
        }

        return RaffleAwardEntity.builder()
                .awardId(awardId)
                .build();
    }

    protected abstract RuleActionEntity<RuleActionEntity.RaffleBeforeEntity> doCheckRaffleBeforeLogic(RaffleFactorEntity raffleFactorEntity, String... logics);


    protected abstract RuleActionEntity<RuleActionEntity.RaffleCenterEntity> doCheckRaffleCenterLogic(RaffleFactorEntity raffleFactorEntity, String... logics);

}
