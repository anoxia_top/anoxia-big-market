package cn.anoxia.domain.strategy.repository;

import cn.anoxia.domain.strategy.model.entity.StrategyAwardEntity;
import cn.anoxia.domain.strategy.model.entity.StrategyEntity;
import cn.anoxia.domain.strategy.model.entity.StrategyRuleEntity;
import cn.anoxia.domain.strategy.model.vo.StrategyAwardRuleModelVO;

import java.util.List;
import java.util.Map;

public interface IStrategyRepository {

    List<StrategyAwardEntity> queryStrategyAwardList(Long strategyId);

    void storeStrategyAwardSearchRateTable(String strategyId, int size, Map<Integer, Integer> shuffleStrategyAwardSearchRateTable);

    StrategyEntity queryStrategyEntityByStrategyId(Long strategyId);

    StrategyRuleEntity queryStrategyRule(Long strategyId, String ruleWeight);

    int getRateRange(Long strategyId);

    int getRateRange(String key);

    Integer getStrategyAwardAssemble(String key, int randomNum);


    String queryStrategyRuleValue(Long strategyId, Integer awardId, String ruleModel);


    String queryStrategyRuleValue(Long strategyId, String ruleModel);

    StrategyAwardRuleModelVO queryStrategyAwardRuleModelVO(Long strategyId, Integer awardId);
}
