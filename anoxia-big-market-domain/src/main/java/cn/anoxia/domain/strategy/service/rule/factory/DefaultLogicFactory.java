package cn.anoxia.domain.strategy.service.rule.factory;

import cn.anoxia.domain.strategy.model.entity.RuleActionEntity;
import cn.anoxia.domain.strategy.service.rule.ILogicFilter;
import cn.anoxia.types.annotations.LogicStrategy;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class DefaultLogicFactory {

    private Map<String, ILogicFilter<?>> logicFilterMap = new ConcurrentHashMap<>();

    public DefaultLogicFactory(List<ILogicFilter<?>> logicFilters) {
        logicFilters.forEach(logicFilter->{
            LogicStrategy logicStrategy = AnnotationUtils.findAnnotation(logicFilter.getClass(), LogicStrategy.class);
            if (logicStrategy != null) {
                logicFilterMap.put(logicStrategy.logicModel().getCode(), logicFilter);
            }
        });
    }


    public <T extends RuleActionEntity.RaffleEntity> Map<String, ILogicFilter<T>> openLogicFilter() {
        return (Map<String, ILogicFilter<T>>) (Map<?, ?>) logicFilterMap;
    }


}
