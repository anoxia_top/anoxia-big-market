package cn.anoxia.domain.strategy.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RuleActionEntity<T extends RuleActionEntity.RaffleEntity> {

    private String ruleModel;

    private T data;

    private String code;

    private String info;


    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class RaffleBeforeEntity extends RaffleEntity{
        private Long strategyId;
        private Integer awardId;
        /**
         * 保底包含的奖品
         */
        private String ruleWeightValueKey;

    }


    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    public static class RaffleCenterEntity extends RaffleEntity {
        private Long strategyId;

        private Integer awardId;
    }


    @Data
    public static class RaffleEntity{
    }

}
