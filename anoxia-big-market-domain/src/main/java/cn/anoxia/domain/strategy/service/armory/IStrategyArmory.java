package cn.anoxia.domain.strategy.service.armory;

public interface IStrategyArmory {

    boolean assembleLotteryStrategy(Long strategyId);

}
