package cn.anoxia.domain.strategy.service.armory;

import cn.anoxia.domain.strategy.model.entity.StrategyAwardEntity;
import cn.anoxia.domain.strategy.model.entity.StrategyEntity;
import cn.anoxia.domain.strategy.model.entity.StrategyRuleEntity;
import cn.anoxia.domain.strategy.repository.IStrategyRepository;
import cn.anoxia.types.enums.ResponseCode;
import cn.anoxia.types.exception.AppException;
import org.checkerframework.checker.units.qual.K;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.SecureRandom;
import java.util.*;

@Service
public class StrategyArmoryDispatch implements IStrategyArmory,IStrategyDispatch{


    @Resource
    private IStrategyRepository strategyRepository;


    @Override
    public boolean assembleLotteryStrategy(Long strategyId) {
        String strategyIdStr = String.valueOf(strategyId);
        // 1.查询策略配置
        List<StrategyAwardEntity> strategyAwardList = strategyRepository.queryStrategyAwardList(strategyId);
        assembleLotteryStrategy(strategyIdStr, strategyAwardList);

        // 2. 权重策略配置 - 适用于 rule_weight 权重规则配置
        StrategyEntity strategyEntity = strategyRepository.queryStrategyEntityByStrategyId(strategyId);
        String ruleWeight = strategyEntity.getRuleWeight();
        if (ruleWeight == null) {
            return true;
        }
        // 3. 获取策略规则
        StrategyRuleEntity strategyRuleEntity = strategyRepository.queryStrategyRule(strategyId, ruleWeight);
        if (strategyRuleEntity == null) {
            throw new AppException(ResponseCode.STRATEGY_RULE_WEIGHT_IS_NULL.getCode(), ResponseCode.STRATEGY_RULE_WEIGHT_IS_NULL.getInfo());
        }
        // 4. 权重策略配置
        Map<String, List<Integer>> ruleWeightValues = strategyRuleEntity.getRuleWeightValues();
        Set<String> ruleKeys = ruleWeightValues.keySet();
        for (String ruleKey : ruleKeys) {
            List<Integer> weightValues = ruleWeightValues.get(ruleKey);
            ArrayList<StrategyAwardEntity> strategyAwardEntitiesClone = new ArrayList<>(strategyAwardList);
            // 5. 奖品重置，当升级到一个档次后，去除一些不属于这个档次的奖品
            strategyAwardEntitiesClone.removeIf(entity->!weightValues.contains(entity.getAwardId()));
            assembleLotteryStrategy(String.valueOf(strategyId).concat("_").concat(ruleKey), strategyAwardEntitiesClone);
        }
        return true;
    }


    /**
     * 生成奖品的数量，然后打乱顺序，放进redis
     * 抽取的时候，直接随机获取一个数字，对应的就是奖品的id
     * @param key
     * @param strategyAwardEntities
     */
    private void assembleLotteryStrategy(String key, List<StrategyAwardEntity> strategyAwardEntities) {
        // 1.查询策略配置
        // 2.获取最小概率值
        BigDecimal minAwardRate = strategyAwardEntities.stream()
                .map(StrategyAwardEntity::getAwardRate)
                .min(BigDecimal::compareTo)
                .orElse(BigDecimal.ZERO);
        // 3.获取概率总和
        BigDecimal totalAwardRate = strategyAwardEntities.stream()
                .map(StrategyAwardEntity::getAwardRate)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        // 4. 用 1 % 0.0001 获得概率范围，百分位、千分位、万分位  计算 抽奖的总数
        BigDecimal rateRange = totalAwardRate.divide(minAwardRate, 4, RoundingMode.CEILING);
        // 5. 填充每个奖品所占的范围 计算每个奖品的个数
        List<Integer> strategyAwardSearchRateTables = new ArrayList<>(rateRange.intValue());
        for (StrategyAwardEntity strategyAward : strategyAwardEntities) {
            Integer awardId = strategyAward.getAwardId();
            BigDecimal awardRate = strategyAward.getAwardRate();
            for (int i = 0; i < rateRange.multiply(awardRate).setScale(0, RoundingMode.CEILING).intValue(); i++) {
                strategyAwardSearchRateTables.add(awardId);
            }
        }
        // 6. 对奖品进行乱序 奖品随机排序
        Collections.shuffle(strategyAwardSearchRateTables);
        // 7. 生成出Map集合，key值，对应的就是后续的概率值。通过概率来获得对应的奖品ID
        Map<Integer, Integer> shuffleStrategyAwardSearchRateTable = new HashMap<>();
        for (int i = 0; i < strategyAwardSearchRateTables.size(); i++) {
            shuffleStrategyAwardSearchRateTable.put(i, strategyAwardSearchRateTables.get(i));
        }
        // 8. 存放到redis
        strategyRepository.storeStrategyAwardSearchRateTable(key, shuffleStrategyAwardSearchRateTable.size(), shuffleStrategyAwardSearchRateTable);
    }

    @Override
    public Integer getRandomAwardId(Long strategyId) {
        // 分布式部署下，不一定为当前应用做的策略装配。也就是值不一定会保存到本应用，而是分布式应用，所以需要从 Redis 中获取。
        int rateRange = strategyRepository.getRateRange(strategyId);
        // 通过生成的随机值，获取概率值奖品查找表的结果
        return strategyRepository.getStrategyAwardAssemble(String.valueOf(strategyId), new SecureRandom().nextInt(rateRange));
    }

    @Override
    public Integer getRandomAwardId(Long strategyId, String ruleWeightValue) {
        String key = String.valueOf(strategyId).concat("_").concat(ruleWeightValue);
        // 分布式部署下，不一定为当前应用做的策略装配。也就是值不一定会保存到本应用，而是分布式应用，所以需要从 Redis 中获取。
        int rateRange = strategyRepository.getRateRange(key);
        // 通过生成的随机值，获取概率值奖品查找表的结果
        return strategyRepository.getStrategyAwardAssemble(key, new SecureRandom().nextInt(rateRange));
    }
}
