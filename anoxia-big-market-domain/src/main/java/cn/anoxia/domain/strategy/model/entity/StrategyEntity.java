package cn.anoxia.domain.strategy.model.entity;

import cn.anoxia.types.common.Constants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StrategyEntity {

    /** 抽奖策略ID */
    private Long strategyId;
    /** 抽奖策略描述 */
    private String strategyDesc;
    /** 抽奖规则模型 rule_weight,rule_blacklist */
    private String ruleModels;


    public String[] ruleModels() {
        if (ruleModels == null) {
            return null;
        }
        return ruleModels.split(Constants.SPLIT);
    }


    public String getRuleWeight() {
        String[] ruledModels = this.ruleModels();
        for (String ruled : ruledModels) {
            if ("rule_weight".equals(ruled)) {
                return ruled;
            }
        }
        return null;
    }
}
