package cn.anoxia.domain.strategy.service.rule.impl;

import cn.anoxia.domain.strategy.model.entity.RuleActionEntity;
import cn.anoxia.domain.strategy.model.entity.RuleMatterEntity;
import cn.anoxia.domain.strategy.model.entity.StrategyEntity;
import cn.anoxia.domain.strategy.repository.IStrategyRepository;
import cn.anoxia.domain.strategy.service.rule.ILogicFilter;
import cn.anoxia.types.annotations.LogicStrategy;
import cn.anoxia.types.common.Constants;
import cn.anoxia.types.enums.LogicModel;
import cn.anoxia.types.enums.RuleLogicCheckTypeVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 黑名单过滤规则
 */
@Slf4j
@Component
@LogicStrategy(logicModel = LogicModel.RULE_BLACKLIST)
public class RuleBackListLogicFilter implements ILogicFilter<RuleActionEntity.RaffleBeforeEntity> {

    @Resource
    private IStrategyRepository strategyRepository;

    @Override
    public RuleActionEntity<RuleActionEntity.RaffleBeforeEntity> filter(RuleMatterEntity ruleMatterEntity) {
        log.info("规则过滤-黑名单 userId:{} strategyId:{} ruleModel:{}", ruleMatterEntity.getUserId(), ruleMatterEntity.getStrategyId(), ruleMatterEntity.getRuleModel());
        String userId = ruleMatterEntity.getUserId();
        Integer awardId = ruleMatterEntity.getAwardId();
        Long strategyId = ruleMatterEntity.getStrategyId();
        String ruleModel = ruleMatterEntity.getRuleModel();
        // 查询当前 黑名单抽奖策略值
        String ruleValue = strategyRepository.queryStrategyRuleValue(strategyId, awardId, ruleModel);
        if (StringUtils.isBlank(ruleValue)) {
            // 有值，直接返回
            return RuleActionEntity.<RuleActionEntity.RaffleBeforeEntity>builder()
                    .code(RuleLogicCheckTypeVO.ALLOW.getCode())
                    .info(RuleLogicCheckTypeVO.ALLOW.getInfo())
                    .build();
        }
        // awardId:userId,userId
        String[] splitRuleValue = ruleValue.split(Constants.COLON);
        String backAwardId = splitRuleValue[0];
        String[] userIds = splitRuleValue[1].split(Constants.SPLIT);
        for (String backUserId : userIds) {
            if (backUserId.equals(userId)) {
                return RuleActionEntity.<RuleActionEntity.RaffleBeforeEntity>builder()
                        .ruleModel(LogicModel.RULE_BLACKLIST.getCode())
                        .data(RuleActionEntity.RaffleBeforeEntity.builder()
                                .strategyId(ruleMatterEntity.getStrategyId())
                                .awardId(Integer.parseInt(backAwardId))
                                .build())
                        .code(RuleLogicCheckTypeVO.TAKE_OVER.getCode())
                        .info(RuleLogicCheckTypeVO.TAKE_OVER.getInfo())
                        .build();
            }
        }
        return RuleActionEntity.<RuleActionEntity.RaffleBeforeEntity>builder()
                .code(RuleLogicCheckTypeVO.ALLOW.getCode())
                .info(RuleLogicCheckTypeVO.ALLOW.getInfo())
                .build();
    }
}
