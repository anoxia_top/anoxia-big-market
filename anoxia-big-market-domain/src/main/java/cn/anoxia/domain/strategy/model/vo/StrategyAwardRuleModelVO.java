package cn.anoxia.domain.strategy.model.vo;

import cn.anoxia.types.common.Constants;
import cn.anoxia.types.enums.LogicModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StrategyAwardRuleModelVO {

    private String ruleModels;



    public String[] raffleCenterRuleModelList() {
        return Arrays.stream(ruleModels.split(Constants.SPLIT))
                .filter(LogicModel::isCenter)
                .toArray(String[]::new);
    }


    public String[] raffleAfterRuleModelList() {
        return Arrays.stream(ruleModels.split(Constants.SPLIT))
                .filter(LogicModel::isAfter)
                .toArray(String[]::new);
    }




}
