package cn.anoxia.types.common;

public class Constants {

    public final static String SPLIT = ",";

    public final static String COLON = ":";
    public final static String SPACE = " ";

    public final static String STR_EMPTY = "";


    public static class RedisKey {

        public static String SERVICE_NODE = "anoxia:";

        public static String STRATEGY_KEY = SERVICE_NODE + "big_market_strategy_key_";
        public static String STRATEGY_AWARD_KEY = SERVICE_NODE + "big_market_strategy_award_key_";
        public static String STRATEGY_RATE_TABLE_KEY = SERVICE_NODE + "big_market_strategy_rate_table_key_";
        public static String STRATEGY_RATE_RANGE_KEY = SERVICE_NODE + "big_market_strategy_rate_range_key_";
    }


}
