package cn.anoxia.types.annotations;

import cn.anoxia.types.enums.LogicModel;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface LogicStrategy {

    /**
     * 规则模型
     * @return LogicModel
     */
    LogicModel logicModel();

}
