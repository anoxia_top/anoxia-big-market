package cn.anoxia.types.enums;

import javafx.scene.input.KeyCodeCombination;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RuleLogicCheckTypeVO {

    TAKE_OVER("take_over","【抽奖前规则】结束过滤"),
    ALLOW("allow","【抽奖前规则】通过"),
    ;
    private final String code;

    private final String info;

}
