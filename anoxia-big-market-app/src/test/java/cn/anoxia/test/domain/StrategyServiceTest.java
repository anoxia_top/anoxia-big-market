package cn.anoxia.test.domain;

import cn.anoxia.domain.strategy.model.entity.RaffleAwardEntity;
import cn.anoxia.domain.strategy.model.entity.RaffleFactorEntity;
import cn.anoxia.domain.strategy.repository.IStrategyRepository;
import cn.anoxia.domain.strategy.service.armory.IStrategyArmory;
import cn.anoxia.domain.strategy.service.armory.StrategyArmoryDispatch;
import cn.anoxia.domain.strategy.service.raffle.IRaffleStrategy;
import cn.anoxia.domain.strategy.service.rule.impl.RuleWeightLogicFilter;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import javax.annotation.Resource;

@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
public class StrategyServiceTest {

    @Resource
    private StrategyArmoryDispatch strategyArmoryDispatch;


    @Test
    public void test(){
        boolean assembled = strategyArmoryDispatch.assembleLotteryStrategy(100001L);
        Assert.assertTrue("初始化失败",assembled);
    }


    @Resource
    private RuleWeightLogicFilter ruleWeightLogicFilter;


    @Resource
    private IRaffleStrategy raffleStrategy;


    @Before
    public void setUp(){
        ReflectionTestUtils.setField(ruleWeightLogicFilter, "userScore", 4500L);
    }


    @Test
    public void test_performRaffle() {
        RaffleFactorEntity raffleFactorEntity = RaffleFactorEntity.builder()
                .userId("xiaofuge")
                .strategyId(100001L)
                .build();
        RaffleAwardEntity raffleAwardEntity = raffleStrategy.performRaffle(raffleFactorEntity);
        log.info("请求参数：{}", JSON.toJSONString(raffleFactorEntity));
        log.info("测试结果：{}", JSON.toJSONString(raffleAwardEntity));
    }


    @Test
    public void test_performRaffle_blacklist() {
        RaffleFactorEntity raffleFactorEntity = RaffleFactorEntity.builder()
                .userId("user003")  // 黑名单用户 user001,user002,user003
                .strategyId(100001L)
                .build();
        RaffleAwardEntity raffleAwardEntity = raffleStrategy.performRaffle(raffleFactorEntity);
        log.info("请求参数：{}", JSON.toJSONString(raffleFactorEntity));
        log.info("测试结果：{}", JSON.toJSONString(raffleAwardEntity));
    }


    @Test
    public void testRandom(){
        Integer randomAwardId = strategyArmoryDispatch.getRandomAwardId(100001L);
        log.info("测试结果：{}", randomAwardId);
        log.info("测试结果：{} - 4000 策略配置", strategyArmoryDispatch.getRandomAwardId(100001L, "4000:102,103,104,105"));
        log.info("测试结果：{} - 5000 策略配置", strategyArmoryDispatch.getRandomAwardId(100001L, "5000:102,103,104,105,106,107"));
        log.info("测试结果：{} - 6000 策略配置", strategyArmoryDispatch.getRandomAwardId(100001L, "6000:102,103,104,105,106,107,108,109"));
    }


}
