package cn.anoxia.test.domain;


import cn.anoxia.domain.strategy.model.entity.RaffleAwardEntity;
import cn.anoxia.domain.strategy.model.entity.RaffleFactorEntity;
import cn.anoxia.domain.strategy.service.armory.IStrategyArmory;
import cn.anoxia.domain.strategy.service.raffle.IRaffleStrategy;
import cn.anoxia.domain.strategy.service.rule.impl.RuleLockLogicFilter;
import cn.anoxia.domain.strategy.service.rule.impl.RuleWeightLogicFilter;
import cn.anoxia.types.common.Constants;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.redisson.api.RedissonClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import javax.annotation.Resource;

@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
public class RaffleStrategyTest {


    @Resource
    private IStrategyArmory strategyArmory;

    @Resource
    private RuleWeightLogicFilter ruleWeightLogicFilter;

    @Resource
    private RuleLockLogicFilter ruleLockLogicFilter;

    @Resource
    private IRaffleStrategy raffleStrategy;

    @Resource
    private RedissonClient redissonClient;


    @Before
    public void before(){

        // 策略装配 100001、100002、100003
        log.info("测试结果：{}", strategyArmory.assembleLotteryStrategy(100001L));
        log.info("测试结果：{}", strategyArmory.assembleLotteryStrategy(100002L));
        log.info("测试结果：{}", strategyArmory.assembleLotteryStrategy(100003L));

        // 通过反射 mock 规则中的值
        ReflectionTestUtils.setField(ruleWeightLogicFilter, "userScore", 40500L);
        ReflectionTestUtils.setField(ruleLockLogicFilter, "userRaffleCount", 10L);
    }


    @Test
    public void lottery(){
        RaffleFactorEntity raffleFactorEntity = RaffleFactorEntity.builder()
                .userId("anoxia")
                .strategyId(100003L)
                .build();
        RaffleAwardEntity raffleAwardEntity = raffleStrategy.performRaffle(raffleFactorEntity);
        log.info("请求参数：{}", JSON.toJSONString(raffleFactorEntity));
        log.info("测试结果：{}", JSON.toJSONString(raffleAwardEntity));
    }


    @Test
    public void clearCache(){
        redissonClient.getBucket(Constants.RedisKey.STRATEGY_KEY + 100001L).delete();
        redissonClient.getBucket(Constants.RedisKey.STRATEGY_KEY + 100002L).delete();
        redissonClient.getBucket(Constants.RedisKey.STRATEGY_KEY + 100003L).delete();
    }

    @After
    public void after(){
        redissonClient.getBucket(Constants.RedisKey.STRATEGY_KEY + 100001L).delete();
        redissonClient.getBucket(Constants.RedisKey.STRATEGY_KEY + 100002L).delete();
        redissonClient.getBucket(Constants.RedisKey.STRATEGY_KEY + 100003L).delete();
    }
}
